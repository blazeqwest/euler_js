//* Problem 6
//* The sum of the squares of the first ten natural numbers is,

//* 1**2 + 2**2 + ... + 10*2 = 385
//* The square of the sum of the first ten natural numbers is,

//* (1 + 2 + ... + 10)**2 = 55**2 = 3025
//* Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025  385 = 2640.

//* Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
var sum_sqr = function(num){
    var result = 0;
	for(var i = 1; i <= num; i++){
		result += i * i;
	}
	return result;
};

var sqr_sum = function(num){
	var result = 0;
	for (var i = 1; i<= num; i++){
		result += i;
	}
	return result * result;
};

console.log(sqr_sum(100) - sum_sqr(100));


