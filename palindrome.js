//* Largest palindrome product
//* Problem 4
//* A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 99.
//* 
//* Find the largest palindrome made from the product of two 3-digit numbers.
var pal_check = function(num){
	var reverse = 0;
	var initial = num;
	do{
		reverse *= 10;
		reverse += num % 10;
		num = (num / 10) - (num % 10) / 10; 	
	}
	while(num > 1)
	if (initial === reverse){return true;}
	else {return false;}
}

var generator = function (top){
	var j;
	var i;
	var result = 1;
	var m1, m2;
	
	for (i = 1; i <= top; i++){
		for (j = 1; j <= top; j++){
			//* if (pal_check(i * j)){console.log(i * j);}
			if ((pal_check(i * j)) && (result <(i * j))){
				result = i * j;
				m1 = i;
				m2 = j;
			}
		}
	}
document.write(result);
document.write("<br>");
document.write(m1);
document.write("<br>");
document.write(m2);
}
generator(999);
// document.write("<br>");
// document.write(pal_check(9009));
// document.write("<br>");


