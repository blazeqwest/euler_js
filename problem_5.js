// Problem 5
// 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
// What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
var dvd = function (top, num){
    var result = 1;
	for (var i = 1; i <=top; i++){
		if (num % i === 0){
			result *= 1;
		}
		else{result *= 0;}
	}
    return result;
};
var i = 1;
while (dvd(20,i) === 0){
	i++;
}
console.log(i);